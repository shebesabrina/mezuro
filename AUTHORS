Mezuro would not be possible without the help of many people involved and the institutions to support it. Here we acknowledge them.

# Active Core Team

      Diego de Araújo Martinez Camarinha (diegoamc at protonmail.ch)
      Eduardo Silva Araújo (duduktamg at hotmail.com)
      Paulo Meirelles (paulo at softwarelivre.org)
      Rafael Reggiani Manzo (rr.manzo at protonmail.com)

# Retired Core Team

      Alessandro Palmeira (alessandro.palmeira at gmail.com)
      Daniel Paulino Alves (danpaulalves at gmail.com)
      Daniel Quadros de Miranda (danielkza2 at gmail.com)
      Fellipe Souto Sampaio (fllsouto at gmail.com)
      Guilherme Rojas (guilhermehrojas at gmail.com)
      Heitor Reis Ribeiro (marcheing at gmail.com)
      João M. M. Silva (jaodsilv at linux.ime.usp.br)
      Pedro Scocco (pedroscocco at gmail.com)
      Renan Fichberg (rfichberg at gmail.com)

# Other contributors through time

    UnB Software Evolution Team 2017
    ---------------------------------------------------------
      Dylan Guedes (djmgguedes at gmail.com)
      Kassia Catarine (kassia_catarine.15 at hotmail.com)
      Marcelo Cristiano (marceloabk at hotmail.com)
      Marcelo Oliveira Martins (martins.oliveira.mo at gmail.com)
      Pedro Ivo de Andrade (andradepedroivo at gmail.com)
      Vitor Barbosa Araijo (vitornga15 at gmail.com)

    UnB Software Evolution Team 2016
    ---------------------------------------------------------
      Dylan Guedes (djmgguedes at gmail.com)
      Jônnatas Lennon (jonatas_lenon at hotmail.com)
      Leonardo Cambraia Corrêa (cambraia at protonmail.com)
      Marcos Antonio Durães Dourado (marcosdourado92 at gmail.com)
      Matheus Ferraz (matheus_ccmf at yahoo.com.br)
      Wilton da Silva Rodrigues (braynwilton at gmail.com)

    UnB Software Evolution Team 2015
    ---------------------------------------------------------
      Álvaro Fernando Souza (alvarofernandoms at gmail.com)
      Eduardo Brasil (brasil.eduardo1 at gmail.com)
      Lucas dos Santos (lucas.srl8 at gmail.com)
      Maria Luciene (marialucienefelix at gmail.com)
      Maxwell Almeida (llewxam150 at gmail.com)
      Parley Pacheco Martins (parleypachecomartins at gmail.com)

    UnB Software Evolution Team 2014
    ---------------------------------------------------------
      Beatriz Rezener (beatrizrezener at gmail.com)
      João Gabriel (joaogabrieldebrittoesilva at gmail.com)
      Larissa Rodrigues (larissax55 at gmail.com)
      Rodrigo Siqueira (rodrigosiqueiramelo at gmail.com)
      Thiago Ribeiro (thiagitosouza at hotmail.com)
      Vinicius Franco (viniciusf.arantes at gmail.com)

    Actual version independent collaborators
    ---------------------------------------------------------
      Arthur del Esposte (arthurmde at gmail.com)
      Renan Costa (renan2727 at hotmail.com)
      Vinicius Vieira (vvieira17 at gmail.com)

    USP Lab XP course 2014
    ---------------------------------------------------------
      Daniel Miranda (danielkza2 at gmail.com)
      Fellipe Souto Sampaio (fllsouto at gmail.com)
      Pedro Bruel (pedro.bruel at gmail.com)
      Pedro Paulo (pedro at vezza.com.br)
      Pedro Scocco (pedroscocco at gmail.com)
      Thiago Kenji Okada <thiago.mast3r at gmail.com>

    USP Lab XP course 2013 kickstarters for the standalone
    ---------------------------------------------------------
      Ademar Lacerda Filho (ademar.mlf at gmail.com)
      Ana Luísa Losnak (analosnak at gmail.com)
      Alessandro Palmeira (alessandro.palmeira at gmail.com)
      Wilson Kazuo Mizutani (kazuo256 at gmail.com)

    Noosfero Plugin (previous version) contributors 2010-2013
    ---------------------------------------------------------

      Ademar Lacerda Filho (ademar.mlf at gmail.com)
      Almir Alves Pereira (almir.sne at gmail.com)
      Alessandro Palmeira (alessandro.palmeira at gmail.com)
      Ana Luísa Losnak (analosnak at gmail.com)
      Andre Casimiro (ah.casimiro at gmail.com)
      Antonio Terceiro (terceiro at colivre.coop.br)
      Caio Salgado (caio.csalgado at gmail.com)
      Carlos Morais (carlos88morais at gmail.com)
      Daniel Alves (danpaulalves at gmail.com)
      Daniela Feitosa (daniela at colivre.coop.br)
      Diego de Araújo Martinez Camarinha (diegoamc90 at gmail.com)
      Everton Santos (everton2x4 at gmail.com)
      Guilherme Rojas (guilhermehrojas at gmail.com)
      Jefferson Fernandes (jeffs.fernandes at gmail.com)
      Joao Machini (joao.machini at gmail.com)
      João da Silva (jaodsilv@linux.ime.usp.br)
      Paulo Meirelles (paulo at softwarelivre.org)
      Pedro Leal (pedrombl at gmail.com)
      Rafael Reggiani Manzo (rr.manzo at gmail.com)
      Rafael Messias (rmmartins at gmail.com)
      Renan Teruo (renanteruoc at gmail.com)
      Rodrigo Souto (rodrigo at colivre.coop.br)
      Wilson Kazuo Mizutani (kazuo256 at gmail.com)

    Collaborators (from USP Lab XP course 2010 on another code)
    -----------------------------------------------------------

      Ana Paula Oliveira dos Santos (anapaulao.santos at gmail.com)
      Lucianna Almeida (lucianna.th at gmail.com)
      Thiago Colucci (ticolucci at gmail.com)
      Vinicius Daros (vinicius.k.daros at gmail.com)
      Viviane Almeida Santos (viviane.almeida at gmail.com)

    Advisors
    --------

     Fabio Kon (fabio.kon at ime.usp.br)
     Alfredo Goldman (gold at ime.usp.br)

# Supporting institutions

    University of São Paulo - USP
    -----------------------------------------------------------
      Institute of Mathematics and Statistics - IME
      FLOSS competence center - CCSL
      NAPSol

    FAPESP
    -----------------------------------------------------------

    CNPq
    -----------------------------------------------------------

    University of Basília - UnB
    -----------------------------------------------------------
      Gama Faculty - FGA

    Brazilian Ministry of Planning, Development and Management
    -----------------------------------------------------------
      Brazilian Public Software
