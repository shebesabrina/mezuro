# Revision history for Mezuro module Packaging

Packaging is the is the set of scripts that package Mezuro projects for distribution.

The version numbers below try to follow the conventions at http://semver.org/.

## Unreleased

* Merge into Mezuro's unique repository
* Upgrade to Ruby 2.4.1
* Update json gem
* Add missing Debian dependencies
* Target packaging to Mezuro's centralized repository
* Target packaging to v5.0.4
* Ignore .bundle/

Packaging is the is the set of scripts that package Mezuro projects for distribution.
Copyright (C) 2015-2018 The Mezuro Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
