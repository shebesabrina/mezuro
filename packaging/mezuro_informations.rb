module MezuroInformations
  VERSION = '5.0.4'
  KALIBRO_PROCESSOR = { info: { version: VERSION, release: '1' },
                        data: { name: 'kalibro-processor',
                                desc: 'Web service for static source code analysis',
                                labels: ['web service', 'code analysis', 'source code metrics'],
                                licenses: ['AGPL-V3'],
                                vcs_url: 'https://gitlab.com/mezuro/mezuro.git',
                                website_url: 'http://mezuro.org',
                                issue_tracker_url: 'https://gitlab.com/mezuro/mezuro/issues',
                                public_download_numbers: true }
                      }
  KALIBRO_CONFIGURATIONS = { info: { version: VERSION, release: '1' },
                             data: { name: 'kalibro-configurations',
                                     desc: 'Web service for managing code analysis configurations',
                                     labels: ['web service', 'source code metrics', 'metric configurations'],
                                     licenses: ['AGPL-V3'],
                                     vcs_url: 'https://gitlab.com/mezuro/mezuro.git',
                                     website_url: 'http://mezuro.org',
                                     issue_tracker_url: 'https://gitlab.com/mezuro/mezuro/issues',
                                     public_download_numbers: true }
                           }
  PREZENTO = { info: { version: VERSION, release: '1' },
               data: { name: 'prezento',
                       desc: 'Collaborative code metrics',
                       labels: ['web interface', 'source code metrics'],
                       licenses: ['AGPL-V3'],
                       vcs_url: 'https://gitlab.com/mezuro/mezuro.git',
                       website_url: 'http://mezuro.org',
                       issue_tracker_url: 'https://gitlab.com/mezuro/mezuro/issues',
                       public_download_numbers: true }
             }

  PREZENTO_NGINX = { info: { version: '0.0.2', release: '1' },
                     data: { name: 'prezento-nginx',
                             desc: "Mounts Prezento on NGINX's port 80",
                             public_download_numbers: true }
                   }
end
