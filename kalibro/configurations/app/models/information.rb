class Information
  VERSION = File.read(File.expand_path("#{Rails.root}/VERSION", __dir__)).strip
  LICENSE = 'AGPLv3'.freeze
  REPOSITORY_URL = 'https://gitlab.com/mezuro/mezuro/tree/master/kalibro/configurations'.freeze

  def self.data
    {version: VERSION, license: LICENSE, repository_url: REPOSITORY_URL}
  end
end
