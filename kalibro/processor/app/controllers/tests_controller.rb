# Actually there is coverage for this class, but its tests erase the data about it
# See: spec/controllers/tests_controller_spec.rb
#
# :nocov:
unless Rails.env == "production"
  require 'database_cleaner'

  class TestsController < ApplicationController
    def clean_database
      Processing.all.each { |processing| processing.update!(state: 'CANCELED') }
      ActiveRecord::Base.uncached do # The cache can prodeuce an infinite loop here
        while Delayed::Job.count > 0
          Delayed::Job.where('run_at > ?', Time.now + 1.minute).destroy_all
          sleep(1)
        end
      end

      Rails.cache.clear
      DatabaseCleaner.strategy = :truncation
      DatabaseCleaner.clean

      respond_to do |format|
        format.json { render json: {}, status: :ok }
      end
    end
  end
end
# :nocov:
