require 'likeno'

module KalibroClient
  module KalibroCucumberHelpers
    class Cleaner
      include Likeno::RequestMethods

      attr_reader :address_key

      def initialize(address_key)
        @address_key = address_key
      end

      def address
        Likeno.config[address_key]
      end

      def endpoint
        'tests'
      end

      # :nocov:
      def clean_database
        request('clean_database', {}, :post)
      rescue Likeno::Errors::RequestError => error
        p error.response
        raise error
      end
      # :nocov:
    end
  end
end
