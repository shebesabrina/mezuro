# Revision history for Kolekti Codeclimate PHPMD

The Kolekti Codeclimate PHPMD gem implements a collector for PHP issues using Codeclimate with Kolekti

## Unreleased

## v5.0.4 - 18/03/2018

* No changes. Other projects have changed

## v5.0.3 - 14/03/2018

* No changes. Other projects have changed

## v5.0.2 - 12/03/2018

* No changes. Other projects have changed

## v5.0.1 - 11/03/2018

* No changes. Other projects have changed

## v5.0.0 - 05/03/2018

* Update to Ruby 2.4.1
* Restrict codeclimate-test-reporter version
* Restrict codeclimate version
* Development on Mezuro's single repository

## v0.0.2 - 23/03/2016

* Fix parser write return value

## v0.0.1 - 23/03/2016

* Initial release

---

Kolekti Codeclimate PHPMD.
Copyright (C) 2015-2018 The Mezuro Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

