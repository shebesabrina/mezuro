# Mezuro

Mezuro project aims provide a platform to compare projects and metric techniques, teach how to use metrics through configurations, analyze code, avoid technical debts, and disseminate code metrics usage and understanding.

To know how to contribute and how to setup a working environment, please refer to the CONTRIBUTING and HACKING files, respectively.

If you want the services running but do not wish to contribute (yet!), there's packages available for Debian Jessie and CentOS 7. For detailed information on how to install and use them, look here.

## Components

Mezuro is composed of by three Rails applications:

1. **Prezento** ([README](prezento/README.md)) is the responsible for providing a web interface for users as well as taking care of authentication
2. **KalibroProcessor** ([README](kalibro/processor/README.rdoc)) does the heavy lifting of invoking the different metric softwares, parsing the results, aggregating through the source tree modules and interpreting them accordingly to each configuration. As you can see this is a lot of responsibilities for a single software, that's why we came up with an abstraction layer called Kolekti:
    1. **Kolekti** ([README](kalibro/kolekti/README.md)) provides a basic interface understood by KalibroProcessor so each metrics software integration can be developed separately in a way that KalibroProcessor has minimal knowledge about each software. We currently have several implementations for Kolekti:
        * **KolektiAnalizo** ([README](kalibro/kolekti_analizo/README.md)) is the implementation for [Analizo](http://analizo.org), a Java / C / C++ metric extractor
        * **KolektiMetricFu** ([README](kalibro/kolekti_metricfu/README.md)) is the implementation for [MetricFu](https://github.com/metricfu/metric_fu), a Ruby metric extractor
        * **KolektiRadon** ([README](kalibro/kolekti_radon/README.md)) is the implementation for [Radon](https://pypi.python.org/pypi/radon), a Python metric extractor
        * **KolektiCCPHPMD** ([README](kalibro/kolekti_cc_phpmd/README.md)) is the implementation for [PHPMD](https://phpmd.org), a PHP metric extractor
3. **KalibroConfigurations** ([README](kalibro/configurations/README.md)) takes care of managing what metrics shall be gathered for each repository, how they will be aggregated and what interpretation will be given for each value obtained

## Getting Started

Except for non-Ruby Kolekti implementations, you'll need:

* Ruby
  - Each project has its `.ruby-version` file which will specify the expected Ruby version
  - [RVM](rvm.io) is a good option for managing several Ruby projects concurrently
* Postgresql
  - Prezento, KalibroProcessor and KalibroConfigurations require an user to be created. Check their respective files at `config/database.yml`

After that you can setup each project by running `./bin/setup`.

## Contributing

First check the wiki pages on:

* [Development workflow](https://gitlab.com/mezuro/mezuro/wikis/Development-workflow)
* [Standards](https://gitlab.com/mezuro/mezuro/wikis/Standards)

After that pick an issue, ask questions about it and open your merge request. We have a [newcomers](https://gitlab.com/mezuro/mezuro/issues?label_name%5B%5D=newcomers) label for issues that are more indicated for new contributors to get used to the project.

## Code status

TODO

## License

Mezuro is composed of several projects each with its own licensing. We'll list below the license for each one, but **if not specified the default license is the MIT**.

| Project                                                    | License                                                   |
| ---------------------------------------------------------- | --------------------------------------------------------- |
| [Prezento](prezento/COPYING)                               | [AGPLv3](https://www.gnu.org/licenses/agpl.html)          |
| [KalibroProcessor](kalibro/processor/COPYING)              | [AGPLv3](https://www.gnu.org/licenses/agpl.html)          |
| [KalibroConfigurations](kalibro/configurations/COPYING)    | [AGPLv3](https://www.gnu.org/licenses/agpl.html)          |
| [KalibroClient](kalibro/client/COPYING.LESSER)             | [LGPLv3](https://www.gnu.org/licenses/lgpl.html)          |
| [Kolekti](kalibro/kolekti/COPYING.LESSER)                  | [LGPLv3](https://www.gnu.org/licenses/lgpl.html)          |
| [KolektiAnalizo](kalibro/kolekti_analizo/LICENSE)          | [LGPLv3](https://www.gnu.org/licenses/lgpl.html)          |
| [KolektiCCPHPMD](kalibro/kolekti_cc_phpmd/COPYING.lesser)  | [LGPLv3](https://www.gnu.org/licenses/lgpl.html)          |
| [KolektiMetricFu](kalibro/kolekti_metricfu/COPYING.lesser) | [LGPLv3](https://www.gnu.org/licenses/lgpl.html)          |
| [KolektiRadon](kalibro/kolekti_radon/COPYING.LESSER)       | [LGPLv3](https://www.gnu.org/licenses/lgpl.html)          |
| [Packaging](packaging/COPYING)                             | [GPLv3](https://www.gnu.org/licenses/gpl.html)            |
| [Pages](mezuro.gitlab.io/COPYING)                          | [GPLv3](https://www.gnu.org/licenses/gpl.html)            |
| [Pages images](mezuro.gitlab.io/COPYING-IMAGES)            | [CC0](https://creativecommons.org/publicdomain/zero/1.0/) |
